package org.apache.derby.dbcache.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisConnection
{
    private static JedisPool jedisPool;

    public Jedis getJedis()
    {
        if(jedisPool == null)
            initJedisPool();
        return jedisPool.getResource();
    }

    public void close()
    {
        jedisPool.close();
    }

    private void initJedisPool()
    {
/*        JedisPoolConfig jedisPoolConfig= new JedisPoolConfig();

        /*JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(Integer.valueOf(pro.getProperty("redis.pool.maxTotal")));
        config.setMinIdle(Integer.valueOf(pro.getProperty("redis.pool.minIdle")));
        config.setMaxIdle(Integer.valueOf(pro.getProperty("redis.pool.maxIdle")));
        config.setMaxWaitMillis(Long.valueOf(pro.getProperty("redis.pool.maxWait")));
        config.setTestWhileIdle(Boolean.valueOf(pro.getProperty("redis.pool.testWhileIdle")));
        config.setTestOnBorrow(Boolean.valueOf(pro.getProperty("redis.pool.testOnBorrow")));
        config.setTestOnReturn(Boolean.valueOf(pro.getProperty("redis.pool.testOnReturn")));
        jedisPool = new JedisPool(config, pro.getProperty("redis.ip"),
                Integer.valueOf(pro.getProperty("redis.port")),
                3000,
                pro.getProperty("redis.auth")); */


        /*jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMinIdle(2);
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxWaitMillis(1000);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        jedisPool= new JedisPool(jedisPoolConfig, "localhost", 6379); */
    }
}