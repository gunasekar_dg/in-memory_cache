package org.apache.derby.dbcache.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.params.SetParams;

import java.io.*;
import java.sql.ResultSet;

/** Cache class is core of the Cache Implementation Logic.
 * 1. Creates a connection pool to maintain the jedis connection.
 * 2. Generates the complete query by replacing the ? with parameter values.
 *    It does not work properly if the query has ? as part of the values.
 * 3. Retrieves the ResultSet for the given query.
 *     If the ResultSet is present in the cache, CacheResultSet Object is returned.
 *     Otherwise null value is returned.
 * 4. Sets the ResultSet value for the given query and the ResultSet obtained from the Database.
 *    It iterates over the database ResultSet and creates the CacheResultSet.
 *    CacheResultSet is stored in the cache.
 * 5. Convert the CacheResultSet into byte array. Object cannot stored directly in Redis.
 * 6. Convert the byte array into object.
 *
 *
 */
public class Cache
{
    // To maintain the redis connection pool
    private static JedisPool jedisPool;

    /** This static block initializes the JedisPool with
     * the default values
     */
    static
    {
        JedisPoolConfig jedisPoolConfig= new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMinIdle(2);
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxWaitMillis(1000);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        jedisPool= new JedisPool(jedisPoolConfig, "redis-server", 6379);
    }

    // Singleton instance
    private static Cache cache;

    /** This method retrieves the CacheResultSet from the cache for the given query.
     *  Given query and parameter values are combined to form the complete query.
     *  The complete query is converted into a byte array. Then the byte array is looked up in the cache as a key.
     *  If the value exists for the key in cache, it returns the value in the form of a byte array.
     *  The byte array is converted to CacheResultSet and returned the same to the user.
     *  If the value does not exist, null value is returned.
     *
     * @param SQLTXT SQL query string
     * @param pvsValues parameter value string array
     * @return CacheResult or null
     */
    public ResultSet getResultSetFromCache(String SQLTXT, String[] pvsValues)
    {
        byte[] key= generateKey(SQLTXT, pvsValues);
        Jedis jedis= jedisPool.getResource();
        byte[] value= jedis.get(key);
        jedis.close();
        if(value != null)
            return (ResultSet)byteArrayToObject(value);
        return null;
    }


    /** This method converts the given sql string and parameter values into a byte array.
     * It replaces the occurrences of '?' with the parameter values to form the complete query.
     * The complete query is converted into a byte array.
     * @param key SQL string
     * @param pvsValues String array of parameter values
     * @return byte array of complete query
     */
    private byte[] generateKey(String key, String[] pvsValues)
    {
        // TODO '?' may be part of the sql query itself. Find a point where Derby forms the complete query
        //  and retrieve the same
        // TODO use an efficient way of replacing question marks in the SQLTXT.
        // TODO incase of String type parameters it should be enclosed with quotes on the either side.
        for (String i: pvsValues)
            key= key.replaceFirst("\\?",i );
        return objectToByteArray(key);
    }

    /** This method sets the ResultSet for the query in the cache.
     * The sql string and parameter values are converted to form the complete query.
     * The complete query is converted to a byte array to form the key.
     * CacheResultSet is created from the ResultSet executed in the database and converted into a byte array
     * to form the value.
     * The key and value are stored in the cache.
     * CacheResultSet is returned to the callee.
     *
     * @param resultSet ResultSet from the Database
     * @param SQLTXT SQL string
     * @param pvsValues String array of Parameter values
     * @return CacheResultSet
     */
    public ResultSet setResultSetInCache(ResultSet resultSet, String SQLTXT, String [] pvsValues)
    {
        ResultSet cacheResultSet= getResultSetFromCache(SQLTXT,pvsValues);
        if(cacheResultSet != null)
            return cacheResultSet;
        cacheResultSet= new CacheResultSet(resultSet);
        byte[] key= generateKey(SQLTXT, pvsValues);
        byte[] value= objectToByteArray(cacheResultSet);
        Jedis jedis= jedisPool.getResource();
        //jedis.set(key, value);
        jedis.setex(key, 300, value); 
        //jedis.set(key,value, new SetParams().ex(5000));
        jedis.close();
        return cacheResultSet;
    }

    /** This method converts the byte array into Object.
     *
     * @param value byte array
     * @return Object
     */
    private Object byteArrayToObject(byte[] value)
    {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(value);
        ObjectInput objectInput = null;
        Object output = null;
        try {
            objectInput = new ObjectInputStream(byteArrayInputStream);
            output = objectInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch ( Exception e)
        {
            // TODO
        }
        finally
        {
            try {
                byteArrayInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return output;
    }


    /** This method converts the Object into byte array.
     *
     * @param object Object
     * @return byte array
     */
    private byte[] objectToByteArray(Object object)
    {
        if (object == null)
            return null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutput objectOutput;
        byte[] output = null;

        try {
            objectOutput = new ObjectOutputStream(byteArrayOutputStream);
            objectOutput.writeObject(object);
            objectOutput.flush();
            output = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            // TODO
        }
        finally {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return output;
    }


    /** Private constructor
     *
     */
    private Cache()
    {
//        initJedisPool();
    }

/*
    private void initJedisPool()
    {
        JedisPoolConfig jedisPoolConfig= new JedisPoolConfig();


        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(Integer.valueOf(pro.getProperty("redis.pool.maxTotal")));
        config.setMinIdle(Integer.valueOf(pro.getProperty("redis.pool.minIdle")));
        config.setMaxIdle(Integer.valueOf(pro.getProperty("redis.pool.maxIdle")));
        config.setMaxWaitMillis(Long.valueOf(pro.getProperty("redis.pool.maxWait")));
        config.setTestWhileIdle(Boolean.valueOf(pro.getProperty("redis.pool.testWhileIdle")));
        config.setTestOnBorrow(Boolean.valueOf(pro.getProperty("redis.pool.testOnBorrow")));
        config.setTestOnReturn(Boolean.valueOf(pro.getProperty("redis.pool.testOnReturn")));
        jedisPool = new JedisPool(config, pro.getProperty("redis.ip"),Integer.valueOf(pro.getProperty("redis.port")),
                3000,pro.getProperty("redis.auth"));
        jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMinIdle(2);
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxWaitMillis(1000);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        jedisPool= new JedisPool(jedisPoolConfig, "redis-server", 6379);
    }
*/


    /** This method ensures that the singleton principle is maintained.
     * It checks for the existence of the cache object. If it exists, it returns the same.
     * Otherwise, it creates a new Cache and returns the same.
     *
     * @return Cache
     */
    public static Cache getCache()
    {
        if(cache == null)
            cache= new Cache();
        return cache;
    }

}