package org.apache.derby.dbcache.redis;


import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class CacheResultSetMetaData implements ResultSetMetaData, Serializable
{
    private int columnCount;
    private String[] columnClassName;
    private int[] columnDisplaySize;
    private String[] columnLabel;
    private String[] columnName;
    private int[] columnType;
    private String[] columnTypeName;
    private int[] precision;
    private int[] scale;
    private String[] schemaName; // dbName
    private String[] tableName;
    private boolean[] isAutoIncrement;
    private boolean[] isCaseSensitive;
    private int[] isNullable;


    public CacheResultSetMetaData(ResultSetMetaData resultSetMetaData)
    {
        try
        {
            columnCount= resultSetMetaData.getColumnCount();

            columnClassName= new String[columnCount];
            columnDisplaySize= new int[columnCount];
            columnLabel= new String[columnCount];
            columnName= new String[columnCount];
            columnType= new int[columnCount];
            columnTypeName= new String[columnCount];
            precision= new int[columnCount];
            scale= new int[columnCount];
            schemaName= new String[columnCount]; // dbName
            tableName= new String[columnCount];
            isAutoIncrement= new boolean[columnCount];
            isCaseSensitive= new boolean[columnCount];
            isNullable= new int[columnCount];

            int col= columnCount+1;
            for(int i= 1,j= 0; i < col; i++, j++) {
                columnClassName[j] = resultSetMetaData.getColumnClassName(i);
                columnDisplaySize[j] = resultSetMetaData.getColumnDisplaySize(i);
                columnLabel[j] = resultSetMetaData.getColumnLabel(i);
                columnName[j]= resultSetMetaData.getColumnName(i);
                columnType[j]= resultSetMetaData.getColumnType(i);
                columnTypeName[j]= resultSetMetaData.getColumnTypeName(i);
                precision[j]= resultSetMetaData.getPrecision(i);
                scale[j]= resultSetMetaData.getScale(i);
                schemaName[j]= resultSetMetaData.getSchemaName(i);
                tableName[j]= resultSetMetaData.getTableName(i);
                isAutoIncrement[j]= resultSetMetaData.isAutoIncrement(i);
                isCaseSensitive[j]= resultSetMetaData.isCaseSensitive(i);
                isNullable[j]= resultSetMetaData.isNullable(i);
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


    protected void checkForValidColumnIndex(int column) throws SQLException
    {
        if (column < 0 || column > columnCount) {
            System.out.println(column + "\t" + columnCount);
            throw new SQLException();
        }
    }

    @Override
    public int getColumnCount() throws SQLException
    {
        return columnCount;
    }

    @Override
    public boolean isAutoIncrement(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return isAutoIncrement[column-1];
    }

    @Override
    public boolean isCaseSensitive(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return isCaseSensitive[column-1];
    }

    @Override
    public boolean isSearchable(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return true;
    }

    // TODO check this mehtod
    @Override
    public boolean isCurrency(int column) throws SQLException
    {
        return false;
    }

    @Override
    public int isNullable(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return isNullable[column-1];
    }

    // TODO not completed
    @Override
    public boolean isSigned(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return false;
    }

    @Override
    public int getColumnDisplaySize(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnDisplaySize[column-1];
    }

    @Override
    public String getColumnLabel(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnLabel[column-1];
    }

    @Override
    public String getColumnName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnName[column-1];
    }

    @Override
    public String getSchemaName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return schemaName[column-1];
    }

    @Override
    public int getPrecision(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return precision[column-1];
    }

    @Override
    public int getScale(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return scale[column-1];
    }

    @Override
    public String getTableName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return tableName[column-1];
    }

    @Override
    public String getCatalogName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return "";
    }

    @Override
    public int getColumnType(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnType[column-1];
    }

    @Override
    public String getColumnTypeName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnTypeName[column-1];
    }


    // TODO check this method
    @Override
    public boolean isReadOnly(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return true;
    }


    // TODO check this method
    @Override
    public boolean isWritable(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return false;
    }


    // TODO check this method
    @Override
    public boolean isDefinitelyWritable(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return false;
    }

    @Override
    public String getColumnClassName(int column) throws SQLException
    {
        checkForValidColumnIndex(column);
        return columnClassName[column-1];
    }

    // TODO check this method
    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        return null;
    }


    // TODO check this method
    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}

