import java.sql.*;

public class Client
{
    public static void main(String[] args)
    {
        // Database Name
        String dbName="jdbcDemoDB";

        //Connection url
        String connectionURL = "jdbc:derby://localhost:1527/" + dbName + ";create=true";

        // Creating connection, preparedStatement and resultSet
        Connection connection = null;
        PreparedStatement preparedStatement= null;
        ResultSet resultSet= null;

        try
        {
            // obtaining the connection for the database
            connection = DriverManager.getConnection(connectionURL);

            // creating the preparedStatement from the connection

	    System.out.println("Creating preparedStatement");
        preparedStatement = connection.prepareStatement("select * from ID where ID = ? and NAME = ?");
//        preparedStatement = connection.prepareStatement("select * from ID where ID = 1 and NAME ="+ "'sairam'");


        // setting the parameters
        preparedStatement.setInt(1,1);
        preparedStatement.setString(2, "sairam");

            // executing the preparedStatement
	    System.out.println("Executing preparedStatement");
            boolean hasResults = preparedStatement.execute();

	    if(hasResults) resultSet = preparedStatement.getResultSet();
            
            // iterating over the ResultSet
	    System.out.println("Iterating over the ResultSet");
            while (resultSet.next())
                System.out.println(resultSet.getInt(1) + "\t" + resultSet.getString(2));


            // closing the connections
            resultSet.close();
            preparedStatement.close();
            connection.close();


            try
            {
                DriverManager.getConnection("jdbc:derby:;shutdown=true");
            } catch (SQLException se)  {	   }

        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }
}
