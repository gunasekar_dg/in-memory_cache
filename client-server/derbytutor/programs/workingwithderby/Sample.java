import java.sql.*;

public class Sample
{
    public static void main(String[] args)
   {
      String dbName="jdbcDemoDB";
      String connectionURL = "jdbc:derby://localhost:1527/" + dbName + ";create=true";

      Connection conn = null;
      Statement s;
      PreparedStatement psInsert;
      ResultSet myWishes;
      String printLine = "  __________________________________________________";
      String createString = "CREATE TABLE WISH_LIST  "
        +  "(WISH_ID INT NOT NULL GENERATED ALWAYS AS IDENTITY " 
        +  "   CONSTRAINT WISH_PK PRIMARY KEY, " 
        +  " ENTRY_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP, "
        +  " WISH_ITEM VARCHAR(32) NOT NULL) " ;
      String answer= "sairam";

     try 
     {
            conn = DriverManager.getConnection(connectionURL);		 
            System.out.println("Connected to database " + dbName);
            
            s = conn.createStatement();
            System.out.println (" . . . . creating table WISH_LIST");
            s.execute(createString);
            psInsert = conn.prepareStatement("insert into WISH_LIST(WISH_ITEM) values (?)");

            psInsert.setString(1,answer);
            psInsert.executeUpdate();  

            
            myWishes = s.executeQuery("select ENTRY_DATE, WISH_ITEM from WISH_LIST order by ENTRY_DATE");

            
            System.out.println(printLine);
            while (myWishes.next())
            {
              System.out.println("On " + myWishes.getTimestamp(1) + " I wished for " + myWishes.getString(2));
            }
            System.out.println(printLine);
            myWishes.close();
             psInsert.close();
             s.close();
            conn.close();						
            System.out.println("Closed connection");

            //   ## DATABASE SHUTDOWN SECTION ## 
            /*** In embedded mode, an application should shut down Derby.
               Shutdown throws the XJ015 exception to confirm success. ***/			

               try {
                  DriverManager.getConnection("jdbc:derby:;shutdown=true");
               } catch (SQLException se)  {	
                  }
               
        
            
         //  Beginning of the primary catch block: prints stack trace
         }  catch (Throwable e)  {   
            /*       Catch all exceptions and pass them to 
             *       the Throwable.printStackTrace method  */
            System.out.println(" . . . exception thrown:");
            e.printStackTrace(System.out);
         }
         System.out.println("Getting Started With Derby JDBC program ending.");
      }
}
