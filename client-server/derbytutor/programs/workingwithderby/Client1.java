import java.sql.*;

public class Client1
{
    public static void main(String[] args)
   {
      String dbName="jdbcDemoDB";
      String connectionURL = "jdbc:derby://localhost:1527/" + dbName + ";create=true";

      Connection conn = null;
      PreparedStatement psInsert;

     try 
     {
       conn = DriverManager.getConnection(connectionURL);		 
       System.out.println("Connected to database " + dbName);
            
       System.out.println("Going the execute the Statement\n");
       psInsert = conn.prepareStatement("select * from ID where ID = ?");
       System.out.println("Executed the Statement\n");
       psInsert.setInt(1,1);

            
       psInsert.close();
       conn.close();						
       System.out.println("Closed connection");

       try 
       {
         DriverManager.getConnection("jdbc:derby:;shutdown=true");
       } catch (SQLException se)  {	   }
               
     }
     catch (Throwable e)  
     {
       e.printStackTrace(System.out);
     }
     System.out.println("Getting Started With Derby JDBC program ending.");
  }
}
