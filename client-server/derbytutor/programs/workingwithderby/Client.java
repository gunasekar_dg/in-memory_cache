import java.sql.*;

public class Client
{
    public static void main(String[] args)
   {
      String dbName="jdbcDemoDB";
      String connectionURL = "jdbc:derby://localhost:1527/" + dbName + ";create=true";

      Connection conn = null;
      Statement s;
      PreparedStatement psInsert;
      //String createString = "CREATE TABLE ID "
        //+  "(ID INT NOT NULL PRIMARY KEY, NAME VARCHAR(10))";

     try 
     {
       conn = DriverManager.getConnection(connectionURL);		 
       System.out.println("Connected to database " + dbName);
            
       s = conn.createStatement();
       //System.out.println (".... Creating table");
       //s.execute(createString);
       System.out.println("Going the execute the Statement\n");
       psInsert = conn.prepareStatement("select * from ID where ID = ?");
       //String value= "sairam";
       //String sql= "insert into ID (ID, NAME) values ( 2," + "123" + ")";
       System.out.println("Executed the Statement\n");
       //s.executeUpdate(sql);
       psInsert.setInt(1,5);
       //psInsert.setString(2,"sairam");
       //psInsert.executeUpdate();  

            
       psInsert.close();
       s.close();
       conn.close();						
       System.out.println("Closed connection");

       //   ## DATABASE SHUTDOWN SECTION ## 
       /** In embedded mode, an application should shut down Derby.
          Shutdown throws the XJ015 exception to confirm success. ***/			

       try 
       {
         DriverManager.getConnection("jdbc:derby:;shutdown=true");
       } catch (SQLException se)  {	   }
               
         //  Beginning of the primary catch block: prints stack trace
     }
     catch (Throwable e)  
     {
       /** Catch all exceptions and pass them to 
           the Throwable.printStackTrace method  */
            System.out.println(" . . . exception thrown:");
            e.printStackTrace(System.out);
     }
         System.out.println("Getting Started With Derby JDBC program ending.");
  }
}
